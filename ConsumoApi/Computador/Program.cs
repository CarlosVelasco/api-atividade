﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;

namespace ComputadorRequestTest
{
    class Program
    {
        public class Computador
        {
            public string Marca { get; set; }
            public string Modelo { get; set; }
            public int Memoria { get; set; }
            public int SSD { get; set; }
            public List<string> Softwares { get; set; }
            public Fornecedor Fornecedor { get; set; }
            
            

        }

        public class Fornecedor
        {
            public string RSocial { get; set; }
            public string Telefone { get; set; }
            public string Endereco{ get; set; }

        }

        static void Main(string[] args)
        {
            // url base da API
            string url = "http://senacao.tk/objetos/computador_array_objeto";

            // consumindo API
            Computador Computador = BuscarComputador(url);
           

            // exibindo dados retornados
            Console.WriteLine(
                string.Format(
                    "Marca: {0} - Modelo {1} - Memoria {2} - SSD {3}",
                    Computador.Marca,
                    Computador.Modelo,
                    Computador.Memoria,
                    Computador.SSD
                    )
                );

            Console.WriteLine("\nProcessador");
            foreach(string op in Computador.Softwares)
            {
                Console.WriteLine("- " + op);
            }

            Console.WriteLine("\nFornecedor\n");
            Console.WriteLine("- " + Computador.Fornecedor.RSocial);
            Console.WriteLine("- " + Computador.Fornecedor.Telefone);
            Console.WriteLine("- " + Computador.Fornecedor.Endereco);



            // mantendo o console aberto
            Console.ReadLine();
        }

        // função destinada consumir a API que retornará os dados de um computador 
        public static Computador BuscarComputador(string url)
        {
            // Instanciando WebClient para chamadas HTTP
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            // Verificando conteúdo retornado
            Console.WriteLine(content);
            Console.WriteLine("\n\n");

            // Convertendo Json para objeto do tipo Address
            Computador Computador = JsonConvert.DeserializeObject<Computador>(content);

            // retornando computador
            return Computador;
        }
    }
}