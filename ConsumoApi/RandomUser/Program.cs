﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
namespace RandomUser
{
    class Program
    {
        public class RandomUser {
           
            public List<Results> Results { get; set; }

        }
        public class Results
        {

            public string Gender { get; set; }
            public Name Name { get; set; }
            public Location Location { get; set; }
            public string Email { get; set; }

        }

        public class Name
        {
            public string Title { get; set; }
            public string First { get; set; }
            public string Last { get; set; }
            
        }

        public class Location
        {
            public Street Street { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Country { get; set; }
            public string Postcode { get; set; }
            public Coordinates Coordinates { get; set; }
            public Timezone TimeZone { get; set; }

        }
        public class Street
        {
            public int Number { get; set; }
            public string Name { get; set; }
        }
        public class Coordinates
        {
            public double Latitude { get; set; }
            public double Longitude { get; set; }
        }
        public class Timezone
        {
            public string Offset { get; set; }
            public string Description { get; set; }
        }

        static void Main(string[] args)
        {
            string Nacionalidade;
            Console.WriteLine("qual nacionalidade?");
            Nacionalidade =Console.ReadLine();
            int a;
            Console.WriteLine("Quantos usuarios irão retornar?");
            a = int.Parse(Console.ReadLine());

            //url base da API
            string url = "https://randomuser.me/api/?nat=" + Nacionalidade +"&"+"results="+ a ;
            
            

            // consumindo API
           RandomUser User = BuscarRandomUser(url);

            foreach(Results i in User.Results){
                Console.WriteLine(string.Format(
                    "Gender : {0}",
                    i.Gender
                    )
                    );
                Console.WriteLine("\nName");
                Console.WriteLine("- " + i.Name.Title);
                Console.WriteLine("- " + i.Name.First);
                Console.WriteLine("- " + i.Name.Last);

                Console.WriteLine("\nLocation");
                Console.WriteLine("nuber: " +i.Location.Street.Number + "  Street: " + i.Location.Street.Name );
                Console.WriteLine("City: " + i.Location.City);
                Console.WriteLine("State: " + i.Location.State);
                Console.WriteLine("Country: " + i.Location.Country);
                Console.WriteLine("Pstecode: " + i.Location.Postcode);
                Console.WriteLine("Latitude: " + i.Location.Coordinates.Latitude);
                Console.WriteLine("Longitude: " + i.Location.Coordinates.Longitude);
                Console.WriteLine("Offset: " + i.Location.TimeZone.Offset);
                Console.WriteLine("Description: " + i.Location.TimeZone.Description);

                Console.WriteLine("\nEmail");
                Console.WriteLine(string.Format (i.Email));

            }


            Console.ReadLine();


        }

        public static RandomUser BuscarRandomUser(string url)
        {
            // Instanciando WebClient para chamadas HTTP
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            // Verificando conteúdo retornado
            Console.WriteLine(content);
            Console.WriteLine("\n\n");

            // Convertendo Json para objeto do tipo Address
            RandomUser User = JsonConvert.DeserializeObject<RandomUser>(content);

            // retornando computador
            return User;
        }
    }
}
