﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
namespace Usuarios
{
    class Program
    {
        public class Usuario
        {
            public string Nome { get; set; }
            public string Email { get; set; }
            public string Telefone { get; set; }
            public List<string> Conhecimentos { get; set; }
            public Enderoco Endereco { get; set; }
            public List<Qualificacao> Qualificacoes { get; set; }
        }
        public class Enderoco
        {
            public string Rua { get; set; }
            public int Numero { get; set; }
            public string Bairro { get; set; }
            public string Cidade { get; set; }
            public string UF { get; set; }

        }
        public class Qualificacao
        {
            public string Nome { get; set; }
            public string Instituicao { get; set; }
            public int Ano { get; set; }
            

        }

        static void Main(string[] args)
        {
            // url base da API
            string url = "http://senacao.tk/objetos/usuario";

            // consumindo API
            Usuario Usuario = BuscarComputador(url);


            // exibindo dados retornados
            Console.WriteLine(string.Format(
                "Nome: {0} - Email{1} - Telefone{2}",
                Usuario.Nome,
                Usuario.Email,
                Usuario.Telefone
                )
                );


            Console.WriteLine("\nConhecimentos");
            foreach (string op in Usuario.Conhecimentos)
            {
                Console.WriteLine("- " + op);
            }



            Console.WriteLine("\nEndereco\n");
            Console.WriteLine(" - " + Usuario.Endereco.Rua);
            Console.WriteLine(" - " + Usuario.Endereco.Numero);
            Console.WriteLine(" - " + Usuario.Endereco.Bairro);
            Console.WriteLine(" - " + Usuario.Endereco.Cidade);
            Console.WriteLine(" - " + Usuario.Endereco.UF);

            Console.WriteLine("\nQualificacoes\n");


            foreach (Qualificacao a in Usuario.Qualificacoes)
            {
                Console.WriteLine("-" + a.Nome);
                Console.WriteLine("-" + a.Instituicao);
                Console.WriteLine("-" + a.Ano);

            }
          
            




            // mantendo o console aberto
            Console.ReadLine();

        }

        // função destinada consumir a API que retornará os dados de um computador 
        public static Usuario BuscarComputador(string url)
        {
            // Instanciando WebClient para chamadas HTTP
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            // Verificando conteúdo retornado
            Console.WriteLine(content);
            Console.WriteLine("\n\n");

            // Convertendo Json para objeto do tipo Address
            Usuario Computador = JsonConvert.DeserializeObject<Usuario>(content);

            // retornando computador
            return Computador;
        }
    }
}
