﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
namespace Veiculos
{
    class Program
    {
       public class Veiculo
        {
            public string Marca { get; set; }
            public string Modelo { get; set; }
            public int Ano { get; set; }
            public int Quilometragem { get; set; }
            public List<string> Opcionais { get; set; }
            public Vendedor Vendedor { get; set; }
            

        }
        public class Vendedor
        {
            public string Nome { get; set; }
            public int Idade { get; set; }
            public string Celular { get; set; }
            public string Cidade { get; set; }
        }
        static void Main(string[] args)
        {
            
            string url = "http://senacao.tk/objetos/veiculo_array_objeto";
            
            Veiculo veiculo = BuscarVeiculo(url);

            
                Console.WriteLine(string.Format(
                "Marca: {0} - Modelo: {1} - Ano: {2} - Quilometragem: {3} ",
                veiculo.Marca,
                veiculo.Modelo,
                veiculo.Ano,
                veiculo.Quilometragem
               
                
                

                )
               );

            Console.WriteLine("\nOpicionais");

            foreach (string op in veiculo.Opcionais)
            {
                Console.WriteLine(" - " + op);
            }

            Console.WriteLine("\nVendedor\n");
            Console.WriteLine("- " + veiculo.Vendedor.Nome);
            Console.WriteLine("- " + veiculo.Vendedor.Idade);
            Console.WriteLine("- " + veiculo.Vendedor.Celular);
            Console.WriteLine("- " + veiculo.Vendedor.Cidade);



            Console.ReadLine();
             
        }

      public static Veiculo BuscarVeiculo(string url)
        {
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            Console.WriteLine(content);
            Console.WriteLine("\n\n");

            Veiculo veiculo = JsonConvert.DeserializeObject<Veiculo>(content);

            

            return veiculo;

           
            
        }
       
    }
}
