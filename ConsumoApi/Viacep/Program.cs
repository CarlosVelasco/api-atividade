﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
namespace Viacep
{
    class Program
    {
        public class Viacep
        {
            public string Cep { get; set; }
            public string Logradouro { get; set; }
            public string Complemento { get; set; }
            public string Bairro { get; set; }
            public string Localidade { get; set; }
            public string UF { get; set; }
            public string Unidade { get; set; }
            public string Ibge { get; set; }
            public string Gia { get; set; }

        }


        static void Main(string[] args)
        {
            int i = 1;
            


          
             while(i > 0) { 
                string cep;
                cep = Console.ReadLine();
                // url base da API
                string url = "https://viacep.com.br/ws/" + cep + "/json/";

                // consumindo API
                Viacep Viacep = BuscarViacep(url);

                Console.WriteLine(string.Format(
                    "Cep: {0} - Logradouro: {1} - Complemento{2} - Bairro: {3} - Localidade: {4} - UF: {5} - Unidade: {6} - Ibge: {7} - Gia: {8} ",
                        Viacep.Cep,
                        Viacep.Logradouro,
                        Viacep.Complemento,
                        Viacep.Bairro,
                        Viacep.Localidade,
                        Viacep.UF,
                        Viacep.Unidade,
                        Viacep.Ibge,
                        Viacep.Gia
                    )
                    );

                i = int.Parse(Console.ReadLine());
             }






        }

        public static Viacep BuscarViacep(string url)
        {
            // Instanciando WebClient para chamadas HTTP
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            // Verificando conteúdo retornado
            Console.WriteLine(content);
            Console.WriteLine("\n\n");

            // Convertendo Json para objeto do tipo Address
            Viacep Viacep = JsonConvert.DeserializeObject<Viacep>(content);

            // retornando computador
            return Viacep;
        }
    }
}
