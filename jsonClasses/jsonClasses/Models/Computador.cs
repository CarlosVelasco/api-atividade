﻿using System;
using System.Collections.Generic;
using System.Text;

namespace jsonClasses.Models
{
    class Computador
    {
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public int Memoria { get; set; }
        public int SSD { get; set; }
        public string Processador { get; set; }
        public List<string> softwares { get; set; }
        public Fornecedor Fornecedor { get; set; }
        public List<Lojas> lojas { get; set; }
        

    }
    
    class Fornecedor
    {
        public string RSocial { get; set; }
        public string Telefone { get; set; }
        public string Endereco { get; set; }
    }

    class Lojas
    {
        public string Cidade { get; set; }
        public string Endereco { get; set; }
        public string Telefone { get;set; }
        

    }




    
}
