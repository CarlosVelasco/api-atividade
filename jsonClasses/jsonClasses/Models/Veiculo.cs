﻿using System;
using System.Collections.Generic;
using System.Text;

namespace jsonClasses.Models
{
    class Veiculo
    {
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public int Ano { get; set; }
        public int Quilometragem { get; set; }
        public List<string> opicionais { get; set; }
        public Vendedor Vendedor { get; set; }

        public List<Revisoes> revisoes { get; set; }

       


      



    }

    class Vendedor
    {
        public string Nome { get; set; }
        public int idade { get; set; }
        public string Celular { get; set; }
        public string cidade { get; set; }
    }

    class Revisoes
    {
        public string Data { get; set; }
        public int KM { get; set; }
        public string Oficina { get; set; } 

    }

}
